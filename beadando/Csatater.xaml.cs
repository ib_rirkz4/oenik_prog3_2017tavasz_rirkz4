﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace beadando
{
    /// <summary>
    /// Interaction logic for Csatater.xaml
    /// </summary>
    public partial class Csatater : Window
    {
        CsataterViewModel VM;
        DispatcherTimer DT;
        
        public Csatater(CsataterViewModel VM)
        {
            InitializeComponent();
            
            this.VM = VM;
            DT = new DispatcherTimer();
            DT.Interval = TimeSpan.FromSeconds(3);
            DT.Tick += DT_Tick;
            DT.Start();
            VM.PropertyChanged += VM_PropertyChanged;
            VM.HIBA += VM_HIBA;
            VM.NYERTES += VM_NYERTES;
            VM.EXIT += VM_EXIT;
            
        }

        private void VM_EXIT(object sender, ExitEventArgs e)
        {
            if (e.Kilep)
            { this.Close(); }
        }

        private void VM_NYERTES(object sender, NyertesEventArgs e)
        {
            if (MessageBox.Show(e.Nyertes,"A Győztes",MessageBoxButton.OK) == MessageBoxResult.OK)
            {
                this.Close();
            } 
        }

        private void DT_Tick(object sender, EventArgs e)
        {
            oldalsavScreen.HibauzenetBeallit("");
        }

        private void VM_HIBA(object sender, HibaOkaEventArgs e)
        {
            oldalsavScreen.HibauzenetBeallit(e.Hiba);
            
        }

        private void VM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            csataterScreen.InvalidateVisual();
            oldalsavScreen.InvalidateVisual();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            VM.KartyaMagassag=csataterScreen.ActualHeight/6;
            csataterScreen.VMBeallit(VM);
            oldalsavScreen.InvalidateVisual();
            oldalsavScreen.VMBeallit(VM);
            oldalsavScreen.HibauzenetBeallit("");

        }

        private void csataterScreen_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            VM.Kijeloles(e.GetPosition(csataterScreen).Y, e.GetPosition(csataterScreen).X);
            csataterScreen.InvalidateVisual();
           
            csataterScreen.InvalidateVisual();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void oldalsavScreen_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            VM.OldalsavOlvaso(e.GetPosition(oldalsavScreen).X,e.GetPosition(oldalsavScreen).Y,oldalsavScreen.ActualHeight,oldalsavScreen.ActualWidth);
        }

    
        private void oldalsavScreen_MouseMove(object sender, MouseEventArgs e)
        {
            oldalsavScreen.SajatPakliMereteKiiras(VM.SajatPakliMeretKiirasra(e.GetPosition(oldalsavScreen).X, e.GetPosition(oldalsavScreen).Y, oldalsavScreen.ActualHeight, oldalsavScreen.ActualWidth));
            oldalsavScreen.EllenfelPakliMereteKiiras(VM.EllenfelPakliMeretKiirasra(e.GetPosition(oldalsavScreen).X, e.GetPosition(oldalsavScreen).Y, oldalsavScreen.ActualHeight, oldalsavScreen.ActualWidth));
        }
    }
}
