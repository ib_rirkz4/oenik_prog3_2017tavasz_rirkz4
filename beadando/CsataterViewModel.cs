﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace beadando
{
    public class HibaOkaEventArgs : EventArgs
    {
        string hiba;
        public HibaOkaEventArgs(string hiba)
        {
            this.hiba = hiba;
        }

        public string Hiba
        {
            get
            {
                return hiba;
            }

            set
            {
                hiba = value;
            }
        }
    }
    public class NyertesEventArgs : EventArgs
    {
        string nyertes;
        public NyertesEventArgs(string nyertes)
        {
            this.nyertes = nyertes;
        }

        public string Nyertes
        {
            get
            {
                return nyertes;
            }

            set
            {
                nyertes = value;
            }
        }
    }
    public class ExitEventArgs : EventArgs
    {
        bool kilep;
        public ExitEventArgs(bool kilep)
        {
            this.kilep = kilep;
        }

        public bool Kilep
        {
            get
            {
                return kilep;
            }

            set
            {
                kilep = value;
            }
        }
    }
    public class KartyaXY
    {
        int x;
        int y;
        public KartyaXY(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public int X
        {
            get
            {
                return x;
            }

            set
            {
                x = value;
            }
        }

        public int Y
        {
            get
            {
                return y;
            }

            set
            {
                y = value;
            }
        }
    }

    public class CsataterViewModel : Bindable
    {
        /// <summary>
        /// jatekter tömbben szerepel a saját és ellefél keze és csata tere. 
        /// A tömb 0. sora a saját kéz, 1 sora a saját csapda sorunk, 2. a saját szörny sorunk,
        /// 3. az ellenfél szörny sora, 4. az ellenfél csapda sora és a 5. az ellenfél keze.
        /// Ezért ez egy 6*10-es mátrix lesz mivel maximum 10 kártya lehet a kézben is és kint a játéktér egy egy területén
        /// (csata sor és csapda sor)
        /// Az ellenfél mindig ugyan azt a paklit használja
        /// A paklik és a temedők külön külön listákban vannak tárolva
        /// SoundPlayer player = new SoundPlayer(path);
        /// player.Load();
        ///player.Play();
        /// </summary>
        Kartya[,] jatekter;
        List<Kartya> sajatPakli, sajatTemeto; //a ViewModel-ből kapja
        List<Kartya> ellenfelPakli, ellenfelTemeto; //mindig ugyan azt a paklit használja az ellenfél
        //Kartya sajatKijelolt, ellenfelKijelölt;
        SoundPlayer player = new SoundPlayer("../../kepek/muzsika.wav");
        int sajatKezMeret, ellenfelKezMeret, sajatSzornySzam, ellenfelSzornySzam, sajatCsapdaSzam, ellenfelCsapdaSzam;
        bool sajatKor;
        Random r;
        bool huzhatok;
        double kartyaMagassag = 0;
        double kartyaSzelesseg = 0;
        KartyaXY sajatKijelolt;
        KartyaXY ellenfelKijelolt;
        int sajatEletero, ellenfelEletero;
        public event EventHandler<HibaOkaEventArgs> HIBA;
        public event EventHandler<NyertesEventArgs> NYERTES;
        public event EventHandler<ExitEventArgs> EXIT;
        public bool SajatKor
        {
            get
            {
                return sajatKor;
            }

            set
            {
                sajatKor = value;
            }
        }

        public Kartya[,] Jatekter
        {
            get
            {
                return jatekter;
            }

            set
            {
                jatekter = value;
            }
        }

        public double KartyaSzelesseg
        {
            get
            {
                return KartyaMagassag / 1.5;
            }
        }

        public double KartyaMagassag
        {
            get
            {
                return kartyaMagassag;
            }

            set
            {
                kartyaMagassag = value;
            }
        }

        public KartyaXY SajatKijelolt
        {
            get
            {
                return sajatKijelolt;
            }

            set
            {
                sajatKijelolt = value;
            }
        }

        public KartyaXY EllenfelKijelolt
        {
            get
            {
                return ellenfelKijelolt;
            }

            set
            {
                ellenfelKijelolt = value;
            }
        }

        public int SajatEletero
        {
            get
            {
                return SajatEletero1;
            }

            set
            {
                SajatEletero1 = value;
            }
        }

        public int EllenfelEletero
        {
            get
            {
                return EllenfelEletero1;
            }

            set
            {
                EllenfelEletero1 = value;
            }
        }

        public int SajatEletero1
        {
            get
            {
                return sajatEletero;
            }

            set
            {
                sajatEletero = value;
            }
        }

        public int EllenfelEletero1
        {
            get
            {
                return ellenfelEletero;
            }

            set
            {
                ellenfelEletero = value;
            }
        }

        public CsataterViewModel(ViewModel VM)
        {
            if (VM.SajatPakli.Count == 30)
            {
                player.Load();
                player.Play();
                sajatSzornySzam = sajatCsapdaSzam = ellenfelCsapdaSzam = ellenfelSzornySzam = 0;
                EllenfelEletero = SajatEletero = 20;
                Jatekter = new Kartya[6, 10];
                sajatKezMeret = 0;
                ellenfelKezMeret = 0;
                this.r = VM.R;
                sajatTemeto = ellenfelTemeto = new List<Kartya>();
                sajatPakli = new List<Kartya>();
                foreach (Kartya item in VM.SajatPakli)
                {
                    if (item.Csapda)
                    {
                        sajatPakli.Add(new Kartya(item.Id, item.Nev, item.TamadoEro));
                    }
                    else
                    {
                        sajatPakli.Add(new Kartya(item.Id, item.Nev, item.TamadoEro, item.EletEro));
                    }

                }
                ellenfelPakli = new List<Kartya>();
                foreach (Kartya item in VM.EllenfelPakli)
                {
                    if (item.Csapda)
                    {
                        ellenfelPakli.Add(new Kartya(item.Id, item.Nev, item.TamadoEro));
                    }
                    else
                    {
                        ellenfelPakli.Add(new Kartya(item.Id, item.Nev, item.TamadoEro, item.EletEro));
                    }
                }
                pakliKeveres();
                ///A játék leg elején minden játékos 3 lapot húz fel
                for (int i = 0; i < 4; i++)
                {
                    kartyaHuzas(true);
                    kartyaHuzas(false);
                }


            }
            else
            {
                throw new Exception("pakliMeret");
            }
        }





        private bool vanNalaSzorny()
        {
           
            for (int i = 0; i < ellenfelKezMeret; i++)
            {
                if (!jatekter[5, i].Csapda)
                {
                    return true;
                }
            }

            return false;
        }
        private bool vanNalaCsapda()
        {
            bool van = false;

            for (int i = 0; i < ellenfelKezMeret; i++)
            {
                if (jatekter[5, i].Csapda)
                {
                    return true;
                }
            }

            return van;
        }
        private bool vanTamadokepesSzorny()
        {

            for (int i = 0; i < ellenfelSzornySzam; i++)
            {
                if (jatekter[3, i].Tamadhat)
                {
                    return true;
                }
            }

            return false;
        }
        private bool vanTamadokepesCsapda()
        {

            for (int i = 0; i < ellenfelKezMeret; i++)
            {
                if (jatekter[2, i].Tamadhat)
                {
                    return true;
                }
            }

            return false;
        }

        public void EllenfelLep()//AI, ha beválik megy a BostonDynamics-hez
        {

            int lepesszam = r.Next(1, 6);//Ennyi lépéssel rendelkezik az AI
            int kijatszando;
            bool huzhat = true;
            while (lepesszam != 0 && SajatKor == false)
            {
                
                if (ellenfelKezMeret < 1) //Ha a kezében nincs lap akkor húz egyet 
                {
                    kartyaHuzas(SajatKor);
                    huzhat = false;
                    
                    OPC();
                    
                }
                else if (ellenfelSzornySzam < 3 && vanNalaSzorny()) //ha nincs kint neki szörny és van a kezében szörny akkor kirak egyet
                {
                    while (ellenfelSzornySzam < 3 && vanNalaSzorny())
                    {
                        kijatszando = r.Next(0, ellenfelKezMeret);
                        if (jatekter[5, kijatszando] != null && !jatekter[5, kijatszando].Csapda)
                        {
                            jatekter[3, ellenfelSzornySzam] = jatekter[5, kijatszando];
                            jatekter[5, kijatszando] = jatekter[5, ellenfelKezMeret - 1];
                            jatekter[5, ellenfelKezMeret - 1] = null;
                            ellenfelKezMeret--;
                            ellenfelSzornySzam++;
                            
                            OPC();
                            
                        }
                    }

                }
               else if (ellenfelCsapdaSzam < 1 && vanNalaCsapda() ) //ha nincs kint nála csapda de a kezében van akkor kirak egyet
                {
                    while (ellenfelCsapdaSzam < 1 && ellenfelKezMeret > 0)
                    {
                        kijatszando = r.Next(0, ellenfelKezMeret);
                        if (jatekter[5, kijatszando] != null && jatekter[5, kijatszando].Csapda)
                        {
                            jatekter[4, ellenfelCsapdaSzam] = jatekter[5, kijatszando];
                            jatekter[5, kijatszando] = jatekter[5, ellenfelKezMeret - 1];
                            jatekter[5, ellenfelKezMeret] = null;
                            ellenfelKezMeret--;
                            ellenfelCsapdaSzam++;
                           
                            OPC();
                            
                        }
                    }


                }
              else  if (ellenfelSzornySzam >= 1 && sajatSzornySzam >0)//addig támad amíg van támadni képes szörnye vagy el nem fogy a lépése vagy az ellenfél szörnye
                {
                    int i = sajatSzornySzam-1;
                    while (i >-1 &&sajatSzornySzam >= 1 && lepesszam != 0 && vanTamadokepesSzorny())
                    {
                        if (i > -1 && jatekter[3, i] != null && jatekter[3, i].Tamadhat)
                        {
                            Harc(new KartyaXY(3, i), new KartyaXY(2, r.Next(0, sajatSzornySzam)));
                            
                            OPC();
                            
                        }
                        i--;

                    }
                }
              else  if (sajatSzornySzam < 1)//ha nincs lent szörnyem akkor bekapcsol a FaceHunter üzemmód (csak arcra üt)
                {
                    int i = 0;
                    while (lepesszam != 0 && vanTamadokepesSzorny())
                    {
                        if (jatekter[3, i].Tamadhat)
                        {
                            jatekter[3, i].Tamadhat = false;
                            SajatEletero--;
                            
                            OPC();
                            

                        }
                        i++;

                    }

                }
               else if (ellenfelSzornySzam < 1 && ellenfelCsapdaSzam > 0)//nincs lent szörnye, nem tudott szörnyel támadni, támad csapdával
                {
                    int i = ellenfelCsapdaSzam;
                    while (i > -1 && sajatSzornySzam > 1 && lepesszam > 0 && vanTamadokepesCsapda())
                    {
                        if (i > -1 && jatekter[4, i] != null && jatekter[4, i].Tamadhat )
                        {
                            Harc(new KartyaXY(4, i), new KartyaXY(2, r.Next(0, sajatSzornySzam)));
                            
                            OPC();
                            
                        }
                        i--;


                    }

                }
              else  if (huzhat) //kifogyott minden lehetőségéből ezért húz egy lapot
                {
                    kartyaHuzas(sajatKor);
                    huzhat = false;
                    
                    OPC();
                    
                }
                lepesszam--;
                
                OPC();
                
            }

            SajatKor = true;
            for (int i = 0; i < ellenfelSzornySzam; i++)
            {
                jatekter[3, i].Tamadhat = true;
            }
            for (int i = 0; i < ellenfelCsapdaSzam; i++)
            {
                jatekter[4, i].Tamadhat = true;
            }
        }
        public void kartyaHuzas(bool kiKore)//true = saját körünk, false = az ellenfél köre
        {
            if (kiKore)
            {
                //ha még van hely a kézben akkor húz egy lapot oda és azt kiveszi a pkaliból, ha nincs hely akkor csak kiveszi a pakliból
                if (sajatKezMeret < 10)
                {
                    if (sajatPakli.Count == 0)
                    {
                        SajatEletero--;
                    }
                    else
                    {
                        Jatekter[0, sajatKezMeret] = sajatPakli[0];
                        sajatPakli.Remove(sajatPakli[0]);
                        sajatKezMeret++;
                    }
                }
                else
                {
                    sajatPakli.Remove(sajatPakli[0]);
                }
            }
            else
            {
                if (ellenfelKezMeret < 10)
                {
                    if (ellenfelPakli.Count == 0)
                    {
                        EllenfelEletero--;
                    }
                    else
                    {
                        Jatekter[5, ellenfelKezMeret] = ellenfelPakli[0];
                        ellenfelPakli.Remove(ellenfelPakli[0]);
                        ellenfelKezMeret++;
                    }
                }
                else
                {
                    if (ellenfelPakli.Count == 0)
                    {
                        EllenfelEletero--;
                    }
                    else
                    {
                        ellenfelPakli.Remove(ellenfelPakli[0]);
                    }
                }

            }

        }

        public void pakliKeveres()
        ///A pakli egy véletlen szerű elemét kisseréli egy másik véletlen elemével
        ///30 cserét végezel egy keverés és mind a két paklit megkeveri egyszerre
        ///minden cserénél új randomot generál
        {
            Kartya csere;
            int kezdo;
            int tolas;
            for (int i = 0; i < 30; i++)
            {
                kezdo = r.Next(0, sajatPakli.Count);
                tolas = r.Next(0, sajatPakli.Count);
                csere = sajatPakli[kezdo];
                sajatPakli[kezdo] = sajatPakli[tolas];
                sajatPakli[tolas] = csere;
                kezdo = r.Next(0, ellenfelPakli.Count);
                tolas = r.Next(0, ellenfelPakli.Count);
                csere = ellenfelPakli[kezdo];
                ellenfelPakli[kezdo] = ellenfelPakli[tolas];
                ellenfelPakli[tolas] = csere;
            }
        }
        public string SajatPakliMeretKiirasra(double x, double y,double height,double width)
        {
            if (x > width / 2 - 50 && x < width / 2 + 50 && y > height / 6 * 4 && y < height / 6 * 4 + 150)//pakli
            {
                return "" + sajatPakli.Count + " lapunk van még!";
                
            }
            return "";
        }
        public string EllenfelPakliMeretKiirasra(double x, double y, double height, double width)
        {
            if (x > width / 2 - 50 && x < width / 2 + 50 && y > height / 6 * 1 && y < height / 6 * 1 + 150)//pakli
            {
                return "" + ellenfelPakli.Count + " lapja van még";

            }
            return "";
        }
        public void OldalsavOlvaso(double x, double y, double height, double width)
        {
            if (x > width / 2 - 50 && x < width / 2 + 50 && y > height / 6 * 4 && y < height / 6 * 4 + 150 && huzhatok == true)//pakli
            {
                kartyaHuzas(true);
                huzhatok = false;
                OPC();
            }
            else
            if (x > width / 4 && x < width / 4 + 130 && y > height / 2 - 25 && y < height / 2 +25) //end turn gomb
            {
                kartyaHuzas(true);

                for (int i = 0; i < sajatSzornySzam; i++)
                {
                    jatekter[2, i].Tamadhat = true;
                }
                for (int i = 0; i < sajatCsapdaSzam; i++)
                {
                    jatekter[1, i].Tamadhat = true;
                }
                SajatKor = false;
                EllenfelLep();
                kartyaHuzas(false);
                huzhatok = true;
                OPC();
                if (SajatEletero < 1)
                {
                    NYERTES?.Invoke(this, new NyertesEventArgs("Ellenfél nyert!"));
                }
                if (EllenfelEletero < 1)
                {
                    NYERTES?.Invoke(this, new NyertesEventArgs("Gratulálok, nyertél!"));
                }

            }
            if (x > width / 2 - 25 && x < width / 2 + 25 && y < height - 10 && y > height - 40)
            {
                EXIT?.Invoke(this, new ExitEventArgs(true));
            }

        }
        public void Kijeloles(double x, double y)//igazándiból ez a saját körünk
        {

            int Y = (int)(y / KartyaSzelesseg);
            int X = (int)(x / KartyaMagassag);


            if (X == 0 && sajatKijelolt !=null)//ellenfél kezénke kijelölése
            {
                if (!jatekter[sajatKijelolt.X, sajatKijelolt.Y].Tamadhat) HIBA?.Invoke(this, new HibaOkaEventArgs("Még nem támadhat ez a szörny!"));
                else if (ellenfelSzornySzam > 0) HIBA?.Invoke(this, new HibaOkaEventArgs("Addig nem mehetsz arcra amíg van szörnye!"));
                else { EllenfelEletero--; jatekter[sajatKijelolt.X, sajatKijelolt.Y].Tamadhat = false; sajatKijelolt = null; }
                OPC();


            }
            else if (X == 1 && sajatKijelolt != null)//ellenfél cspadájának kijelölése
            {
                HIBA?.Invoke(this, new HibaOkaEventArgs("Onnan nem tudsz mit kijelölni!"));
            }
            else if (X == 2 && jatekter[5 - X, Y] != null)//ellenség szrönyének kijelölése
            {
                if (sajatKijelolt != null)
                {
                    if (jatekter[sajatKijelolt.X, sajatKijelolt.Y].Tamadhat)
                    {
                        ellenfelKijelolt = new KartyaXY(5 - X, Y);
                        Harc(sajatKijelolt, ellenfelKijelolt);
                        OPC();
                        sajatKijelolt = null;
                        ellenfelKijelolt = null;
                    }
                    else
                    {
                        HIBA?.Invoke(this, new HibaOkaEventArgs("Nem tud még támadni ez a kártya!"));
                    }


                }
            }
            else if (X == 3 && jatekter[5 - X, Y] != null)//saját szörny kijelölése a szörny sávban
            {
                SajatKijelolt = new KartyaXY(5 - X, Y);
            }
            else if (X == 4 && jatekter[5 - X, Y] != null)//saját csapda sávon kártya kijelölése
            {
                SajatKijelolt = new KartyaXY(5 - X, Y);
            }
            else if (X == 5 && jatekter[5 - X, Y] != null)//a saját kezemben duplát kattintok egy lapra akkor azt kirakja a szörny sávba!
            {
                if (SajatKijelolt == null)
                {
                    SajatKijelolt = new KartyaXY(5 - X, Y); ;
                }
                else if (jatekter[sajatKijelolt.X, sajatKijelolt.Y] != jatekter[5 - X, Y])
                {
                    SajatKijelolt = new KartyaXY(5 - X, Y);
                }
                else
                {
                    if (!jatekter[sajatKijelolt.X, sajatKijelolt.Y].Csapda && sajatSzornySzam < 10)
                    {
                        jatekter[2, sajatSzornySzam] = jatekter[5 - X, Y];
                        jatekter[0, Y] = jatekter[0, sajatKezMeret - 1];
                        jatekter[0, sajatKezMeret - 1] = null;
                        sajatKezMeret--;
                        sajatSzornySzam++;
                        SajatKijelolt = null;
                    }
                    else if (jatekter[sajatKijelolt.X, sajatKijelolt.Y].Csapda && sajatCsapdaSzam < 10)
                    {
                        jatekter[1, sajatCsapdaSzam] = jatekter[5 - X, Y];
                        jatekter[0, Y] = jatekter[0, sajatKezMeret - 1];
                        jatekter[0, sajatKezMeret - 1] = null;
                        sajatKezMeret--;
                        sajatCsapdaSzam++;
                        SajatKijelolt = null;
                    }
                }
            }


            if (SajatEletero < 1)
            {
                NYERTES?.Invoke(this, new NyertesEventArgs("Ellenfél nyert!"));
            }
            if (EllenfelEletero < 1)
            {
                NYERTES?.Invoke(this, new NyertesEventArgs("Gratulálok, nyertél!"));
            }


        }

        public void Harc(KartyaXY tamado, KartyaXY vedo)
        {
            jatekter[tamado.X, tamado.Y].Tamadhat = false;
            if (jatekter[tamado.X, tamado.Y].Csapda)
            {
                jatekter[vedo.X, vedo.Y].EletEro -= jatekter[tamado.X, tamado.Y].TamadoEro;
                if (tamado.X < 3)//saját csapdám támadott az ellenfél egy szötnyére (csapdára nem lehet támadni, nem nézem külön mit támadtam)
                {
                    sajatTemeto.Add(jatekter[tamado.X, tamado.Y]);//a csapda használat után automatán a temetőbe kerül
                    jatekter[tamado.X, tamado.Y] = jatekter[tamado.X, sajatCsapdaSzam - 1];
                    jatekter[tamado.X, sajatCsapdaSzam - 1] = null;
                    sajatCsapdaSzam--;


                    if (!jatekter[vedo.X, vedo.Y].El)//meghalt az ellenséges szörny is
                    {
                        ellenfelTemeto.Add(jatekter[vedo.X, vedo.Y]);
                        jatekter[vedo.X, vedo.Y] = jatekter[vedo.X, ellenfelSzornySzam - 1];
                        jatekter[vedo.X, ellenfelSzornySzam - 1] = null;
                        ellenfelSzornySzam--;

                    }
                }
                else if (tamado.X > 2)//az cspdája támadott egy szörnyemre
                {
                    ellenfelTemeto.Add(jatekter[tamado.X, tamado.Y]);//a csapda használat után automatán a temetőbe kerül
                    jatekter[tamado.X, tamado.Y] = jatekter[tamado.X, ellenfelCsapdaSzam - 1];
                    jatekter[tamado.X, ellenfelCsapdaSzam - 1] = null;
                    ellenfelCsapdaSzam--;
                    if (!jatekter[vedo.X, vedo.Y].El)//meghalt a saját szörny is
                    {
                        sajatTemeto.Add(jatekter[vedo.X, vedo.Y]);
                        jatekter[vedo.X, vedo.Y] = jatekter[vedo.X, sajatSzornySzam - 1];
                        jatekter[vedo.X, sajatSzornySzam - 1] = null;
                        sajatSzornySzam--;

                    }
                }
            }
            else//szörny támad szörnyet
            {
                if (tamado.X < 3)//saját szörnyem támadott az ellenfél egy szötnyére 
                {
                    jatekter[tamado.X, tamado.Y].EletEro -= jatekter[vedo.X, vedo.Y].TamadoEro;//egymásnka esnke a sszörnyek
                    jatekter[vedo.X, vedo.Y].EletEro -= jatekter[tamado.X, tamado.Y].TamadoEro;
                    if (!jatekter[tamado.X, tamado.Y].El)
                    {
                        sajatTemeto.Add(jatekter[tamado.X, tamado.Y]);//meghalt a szörnyem
                        jatekter[tamado.X, tamado.Y] = jatekter[tamado.X, sajatSzornySzam - 1];
                        jatekter[tamado.X, sajatSzornySzam - 1] = null;
                        sajatSzornySzam--;
                    }
                    if (!jatekter[vedo.X, vedo.Y].El)//meghalt az ellenséges szörny is
                    {
                        ellenfelTemeto.Add(jatekter[vedo.X, vedo.Y]);
                        jatekter[vedo.X, vedo.Y] = jatekter[vedo.X, ellenfelSzornySzam - 1];
                        jatekter[vedo.X, ellenfelSzornySzam - 1] = null;
                        ellenfelSzornySzam--;

                    }
                }
                if (tamado.X > 2)//ellenfél szörnye támadott rám
                {
                    jatekter[tamado.X, tamado.Y].EletEro -= jatekter[vedo.X, vedo.Y].TamadoEro;//egymásnka esnke a sszörnyek
                    jatekter[vedo.X, vedo.Y].EletEro -= jatekter[tamado.X, tamado.Y].TamadoEro;
                    if (!jatekter[tamado.X, tamado.Y].El)
                    {
                        ellenfelTemeto.Add(jatekter[tamado.X, tamado.Y]);//meghalt a szörnye
                        jatekter[tamado.X, tamado.Y] = jatekter[tamado.X, ellenfelSzornySzam - 1];
                        jatekter[tamado.X, ellenfelSzornySzam - 1] = null;
                        ellenfelSzornySzam--;
                    }
                    if (!jatekter[vedo.X, vedo.Y].El)//meghalt a saját szörnyem is
                    {
                        ellenfelTemeto.Add(jatekter[vedo.X, vedo.Y]);
                        jatekter[vedo.X, vedo.Y] = jatekter[vedo.X, sajatSzornySzam - 1];
                        jatekter[vedo.X, sajatSzornySzam - 1] = null;
                        sajatSzornySzam--;

                    }
                }


            }


        }
    }

}
