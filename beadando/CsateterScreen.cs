﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace beadando
{
    class CsateterScreen : FrameworkElement
    {
        CsataterViewModel VM;
        Brush sav = new SolidColorBrush(Colors.Brown);
        ImageBrush ellenfelHatter;
        public void VMBeallit(CsataterViewModel VM)
        {
            this.VM = VM;
            this.InvalidateVisual();
            
        }
        

        protected override void OnRender(DrawingContext drawingContext)
        {
            drawingContext.DrawRectangle(Brushes.LightGray, null, new Rect(0, 0, this.ActualWidth, this.ActualHeight));
            ellenfelHatter = new ImageBrush();
            ellenfelHatter.ImageSource = new BitmapImage(new Uri("../../kepek/ellenfelHatlap.jpg", UriKind.Relative));
            if (VM != null)
            {

                for (int x = 0; x < VM.Jatekter.GetLength(0); x++)
                {

                    for (int y = 0; y < VM.Jatekter.GetLength(1); y++)
                    {
                        sav = new SolidColorBrush(Colors.Brown);
                        if (VM.SajatKijelolt != null && x == VM.SajatKijelolt.X && y == VM.SajatKijelolt.Y)
                        {
                            sav = new SolidColorBrush(Colors.Red);
                        }
                        if (VM.EllenfelKijelolt != null && x == VM.EllenfelKijelolt.X && y == VM.EllenfelKijelolt.Y)
                        {
                            sav = new SolidColorBrush(Colors.Red);
                        }
                        if (x == 0) // sajét kéz
                        {
                            if (VM.Jatekter[x, y] != null)
                            {
                                
                                //kartyahatterszin
                                drawingContext.DrawRoundedRectangle(VM.Jatekter[x, y].KartyaHatter,
                                new Pen(sav, 5),
                                new Rect(y * VM.KartyaSzelesseg, this.ActualHeight - VM.KartyaMagassag, VM.KartyaSzelesseg, VM.KartyaMagassag), 10, 10);
                                //kartyanev
                                drawingContext.DrawText(
                                    new FormattedText(VM.Jatekter[x, y].Nev, new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Segoe UI"), (int)(VM.KartyaMagassag / 10 + 1), Brushes.Green),
                                    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 11, this.ActualHeight - (VM.KartyaMagassag) + (VM.KartyaMagassag / 9) - VM.KartyaMagassag / 10));
                                //kartyaTamadoero
                                drawingContext.DrawText(
                                    new FormattedText(VM.Jatekter[x, y].TamadoEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Red),
                                    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8, this.ActualHeight - (VM.KartyaMagassag) + VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                                //kartyaeletero
                                drawingContext.DrawText(
                                    new FormattedText(VM.Jatekter[x, y].EletEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Green),
                                    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8 * 5, this.ActualHeight - (VM.KartyaMagassag) + VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                                //kartyakép
                                drawingContext.DrawImage(VM.Jatekter[x, y].Kep.ImageSource,
                                    new Rect(y * VM.KartyaSzelesseg + VM.KartyaSzelesseg / 8, this.ActualHeight - VM.KartyaMagassag + VM.KartyaMagassag / 6, VM.KartyaSzelesseg / 8 * 6, VM.KartyaMagassag / 6 * 4));



                            }

                        }
                        if (x == 1)//saját csapdák
                        {


                            if (VM.Jatekter[x, y] != null)
                            {
                                //kartyahatterszin
                                drawingContext.DrawRoundedRectangle(VM.Jatekter[x, y].KartyaHatter,
                                new Pen(sav, 5),
                                new Rect(y * VM.KartyaSzelesseg, this.ActualHeight - VM.KartyaMagassag * (x + 1), VM.KartyaSzelesseg, VM.KartyaMagassag), 10, 10);
                                //kartyanev
                                drawingContext.DrawText(
                                    new FormattedText(VM.Jatekter[x, y].Nev, new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Segoe UI"), (int)(VM.KartyaMagassag / 10 + 1), Brushes.Green),
                                    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 11, this.ActualHeight - (VM.KartyaMagassag * (x + 1)) + (VM.KartyaMagassag / 9) - VM.KartyaMagassag / 10));
                                //kartyaTamadoero
                                drawingContext.DrawText(
                                    new FormattedText(VM.Jatekter[x, y].TamadoEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Red),
                                    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8, this.ActualHeight - (VM.KartyaMagassag * (x + 1)) + VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                                //kartyaeletero
                                drawingContext.DrawText(
                                    new FormattedText(VM.Jatekter[x, y].EletEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Green),
                                    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8 * 5, this.ActualHeight - (VM.KartyaMagassag * (x + 1)) + VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                                //kartyakép
                                drawingContext.DrawImage(VM.Jatekter[x, y].Kep.ImageSource,
                                    new Rect(y * VM.KartyaSzelesseg + VM.KartyaSzelesseg / 8, this.ActualHeight - VM.KartyaMagassag * (x + 1) + VM.KartyaMagassag / 6, VM.KartyaSzelesseg / 8 * 6, VM.KartyaMagassag / 6 * 4));




                            }
                        }
                            if (x == 2)//saját szörnyek
                            {


                                if (VM.Jatekter[x, y] != null)
                                {
                                    //kartyahatterszin
                                    drawingContext.DrawRoundedRectangle(VM.Jatekter[x, y].KartyaHatter,
                                    new Pen(sav, 5),
                                    new Rect(y * VM.KartyaSzelesseg, this.ActualHeight - VM.KartyaMagassag * (x + 1), VM.KartyaSzelesseg, VM.KartyaMagassag), 10, 10);
                                    //kartyanev
                                    drawingContext.DrawText(
                                        new FormattedText(VM.Jatekter[x, y].Nev, new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Segoe UI"), (int)(VM.KartyaMagassag / 10 + 1), Brushes.Green),
                                        new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 11, this.ActualHeight - (VM.KartyaMagassag * (x + 1)) + (VM.KartyaMagassag / 9) - VM.KartyaMagassag / 10));
                                    //kartyaTamadoero
                                    drawingContext.DrawText(
                                        new FormattedText(VM.Jatekter[x, y].TamadoEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Red),
                                        new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8, this.ActualHeight - (VM.KartyaMagassag * (x + 1)) + VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                                    //kartyaeletero
                                    drawingContext.DrawText(
                                        new FormattedText(VM.Jatekter[x, y].EletEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Green),
                                        new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8 * 5, this.ActualHeight - (VM.KartyaMagassag * (x + 1)) + VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                                    //kartyakép
                                    drawingContext.DrawImage(VM.Jatekter[x, y].Kep.ImageSource,
                                        new Rect(y * VM.KartyaSzelesseg + VM.KartyaSzelesseg / 8, this.ActualHeight - VM.KartyaMagassag * (x + 1) + VM.KartyaMagassag / 6, VM.KartyaSzelesseg / 8 * 6, VM.KartyaMagassag / 6 * 4));




                                }

                            }
                        if (x == 3)//ellenfél szörnyek
                        {


                            if (VM.Jatekter[x, y] != null)
                            {
                                //kartyahatterszin
                                drawingContext.DrawRoundedRectangle(VM.Jatekter[x, y].KartyaHatter,
                                new Pen(sav, 5),
                                new Rect(y * VM.KartyaSzelesseg, (5-x)*VM.KartyaMagassag, VM.KartyaSzelesseg, VM.KartyaMagassag), 10, 10);
                                //kartyanev
                                drawingContext.DrawText(
                                    new FormattedText(VM.Jatekter[x, y].Nev, new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Segoe UI"), (int)(VM.KartyaMagassag / 10 + 1), Brushes.Green),
                                    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 11, (5 - x) * VM.KartyaMagassag+(VM.KartyaMagassag / 9) - VM.KartyaMagassag / 10));
                                //kartyaTamadoero
                                drawingContext.DrawText(
                                    new FormattedText(VM.Jatekter[x, y].TamadoEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Red),
                                    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8, (5 - x) * VM.KartyaMagassag+ VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                                //kartyaeletero
                                drawingContext.DrawText(
                                    new FormattedText(VM.Jatekter[x, y].EletEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Green),
                                    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8 * 5, (5 - x) * VM.KartyaMagassag+ VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                                //kartyakép
                                drawingContext.DrawImage(VM.Jatekter[x, y].Kep.ImageSource,
                                    new Rect(y * VM.KartyaSzelesseg + VM.KartyaSzelesseg / 8, (5 - x) * VM.KartyaMagassag+ VM.KartyaMagassag / 6, VM.KartyaSzelesseg / 8 * 6, VM.KartyaMagassag / 6 * 4));



                            }

                        }
                        if (x == 4)//ellenfél csapdák
                        {


                            if (VM.Jatekter[x, y] != null)
                            {
                                //kartyahatterszin
                                drawingContext.DrawRoundedRectangle(VM.Jatekter[x, y].KartyaHatter,
                                new Pen(sav, 5),
                                new Rect(y * VM.KartyaSzelesseg, (5-x)*VM.KartyaMagassag, VM.KartyaSzelesseg, VM.KartyaMagassag), 10, 10);
                                //kartyanev
                                drawingContext.DrawText(
                                    new FormattedText(VM.Jatekter[x, y].Nev, new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Segoe UI"), (int)(VM.KartyaMagassag / 10 + 1), Brushes.Green),
                                    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 11, (5 - x) * VM.KartyaMagassag+(VM.KartyaMagassag / 9) - VM.KartyaMagassag / 10));
                                //kartyaTamadoero
                                drawingContext.DrawText(
                                    new FormattedText(VM.Jatekter[x, y].TamadoEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Red),
                                    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8, (5 - x) * VM.KartyaMagassag+ VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                                //kartyaeletero
                                drawingContext.DrawText(
                                    new FormattedText(VM.Jatekter[x, y].EletEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Green),
                                    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8 * 5, (5 - x) * VM.KartyaMagassag+ VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                                //kartyakép
                                drawingContext.DrawImage(VM.Jatekter[x, y].Kep.ImageSource,
                                    new Rect(y * VM.KartyaSzelesseg + VM.KartyaSzelesseg / 8, (5 - x) * VM.KartyaMagassag+ VM.KartyaMagassag / 6, VM.KartyaSzelesseg / 8 * 6, VM.KartyaMagassag / 6 * 4));



                            }

                        }
                        if (x == 5)//ellenfél kéz
                            {


                                if (VM.Jatekter[x, y] != null)
                                {
                                    //kartyahatterszin
                                    drawingContext.DrawRoundedRectangle(ellenfelHatter,
                                    new Pen(sav, 5),
                                    new Rect(y * VM.KartyaSzelesseg, 0, VM.KartyaSzelesseg, VM.KartyaMagassag), 10, 10);
                                    //kartyanev
                                    //drawingContext.DrawText(
                                    //    new FormattedText(VM.Jatekter[x, y].Nev, new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Segoe UI"), (int)(VM.KartyaMagassag / 10 + 1), Brushes.Green),
                                    //    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 11, (VM.KartyaMagassag / 9) - VM.KartyaMagassag / 10));
                                    ////kartyaTamadoero
                                    //drawingContext.DrawText(
                                    //    new FormattedText(VM.Jatekter[x, y].TamadoEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Red),
                                    //    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8, VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                                    ////kartyaeletero
                                    //drawingContext.DrawText(
                                    //    new FormattedText(VM.Jatekter[x, y].EletEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Green),
                                    //    new Point((y * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8 * 5, VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                                    ////kartyakép
                                    //drawingContext.DrawImage(VM.Jatekter[x, y].Kep.ImageSource,
                                    //    new Rect(y * VM.KartyaSzelesseg + VM.KartyaSzelesseg / 8, VM.KartyaMagassag / 6, VM.KartyaSzelesseg / 8 * 6, VM.KartyaMagassag / 6 * 4));



                                }

                            }

                        }







                    }
                }
            }








        }
    }


