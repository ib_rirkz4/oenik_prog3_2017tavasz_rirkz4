﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace beadando
{
    /// <summary>
    /// Interaction logic for DeckBuilder.xaml
    /// </summary>
    public partial class DeckBuilder : Window
    {
        //public DeckBuilderModel DBM;
        public DeckBuilderViewModel DVM;
        public ViewModel VM;
        public DeckBuilder(ViewModel VM)
        {

            InitializeComponent();
            this.VM = VM;
            DVM = new DeckBuilderViewModel(this.ActualWidth, this.ActualHeight, VM);
            screen.VMbeallit(DVM);
            this.osszKartyaCanvas.Height = DVM.OsszMagassag;
            this.sajatCanvas.Height = DVM.SajatMagassag;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DVM.Abl_mag = screen.ActualHeight;
            DVM.Abl_szel = screen.ActualWidth;
            screen_sajat.VMBeallitas(DVM);
            screen_sajat.ReDraw();
            screen.ReDraw();
        }

        private void screen_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DVM.sajathozAd(e.GetPosition(screen).X,e.GetPosition(screen).Y);
            this.sajatCanvas.Height = DVM.SajatMagassag;
            screen_sajat.ReDraw();
        }

        private void screen_sajat_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DVM.sajatbolKivesz(e.GetPosition(screen_sajat).X);
            this.sajatCanvas.Height = DVM.SajatMagassag;
            screen_sajat.ReDraw();
        }

       
    }
}
