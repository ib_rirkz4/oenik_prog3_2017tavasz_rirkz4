﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace beadando
{
    class DeckBuilderSajatScreen :FrameworkElement
    {
        DeckBuilderViewModel VM;
        public void VMBeallitas(DeckBuilderViewModel VM)
        {
            this.VM = VM;
        }
        public void ReDraw()
        {
            this.InvalidateVisual();
        }
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (VM != null)
            {
                for (int i = 0; i < VM.SajatPakli.Count; i++)
                {
                    //kartyahatterszin
                    drawingContext.DrawRoundedRectangle(VM.SajatPakli[i].KartyaHatter,
                    new Pen(Brushes.Brown, 5),
                    new Rect(0, i * VM.KartyaMagassag, VM.KartyaSzelesseg, VM.KartyaMagassag), 10, 10);
                    //kartyanev
                    drawingContext.DrawText(
                        new FormattedText(VM.SajatPakli[i].Nev, new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Times New Roman"), (int)(VM.KartyaMagassag / 17 + 1), Brushes.Green),
                        new Point((0) + VM.KartyaSzelesseg / 8, (i * VM.KartyaMagassag) + (VM.KartyaMagassag / 6) - VM.KartyaMagassag / 10));
                    //kartyaTamadoero
                    drawingContext.DrawText(
                        new FormattedText(VM.SajatPakli[i].TamadoEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Times New Roman"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Red),
                        new Point((0) + VM.KartyaSzelesseg / 8, (i * VM.KartyaMagassag) + VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                    //kartyaeletero
                    drawingContext.DrawText(
                        new FormattedText(VM.SajatPakli[i].EletEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Times New Roman"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Green),
                        new Point((0) + VM.KartyaSzelesseg / 8 * 5, (i * VM.KartyaMagassag) + VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                    //kartyakép
                    drawingContext.DrawImage(VM.SajatPakli[i].Kep.ImageSource,
                        new Rect(0 + VM.KartyaSzelesseg / 8, i * VM.KartyaMagassag + VM.KartyaMagassag / 6, VM.KartyaSzelesseg / 8 * 6, VM.KartyaMagassag / 6 * 4));

                }

            }

        }
    }
}
