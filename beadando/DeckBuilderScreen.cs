﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace beadando
{
     class DeckBuilderScreen :FrameworkElement
    {
        DeckBuilderViewModel VM;
        ImageBrush myBrush = new ImageBrush();
        
        public void VMbeallit(DeckBuilderViewModel VM)
        {
            this.VM = VM;
            VM.PropertyChanged += VM_PropertyChanged;

        }

        private void VM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            this.InvalidateVisual();
        }
        public void ReDraw()
        {
            this.InvalidateVisual();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (VM != null)
            {
                for (int x = 0; x < VM.Kartyak.GetLength(0); x++)
                {
                    for (int y = 0; y < VM.Kartyak.GetLength(1); y++)
                    {
                        if (VM.Kartyak[x, y] != null)
                        {
                            //kartyahatterszin
                            drawingContext.DrawRoundedRectangle(VM.Kartyak[x, y].KartyaHatter,
                            new Pen(Brushes.Brown, 5),
                            new Rect(x * VM.KartyaSzelesseg, y * VM.KartyaMagassag, VM.KartyaSzelesseg, VM.KartyaMagassag), 10, 10);
                            //kartyanev
                            drawingContext.DrawText(
                                new FormattedText(VM.Kartyak[x, y].Nev, new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Times New Roman"), (int)(VM.KartyaMagassag / 17 + 1), Brushes.Green),
                                new Point((x * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8, (y * VM.KartyaMagassag) + (VM.KartyaMagassag / 6) - VM.KartyaMagassag / 10));
                            //kartyaTamadoero
                            drawingContext.DrawText(
                                new FormattedText(VM.Kartyak[x, y].TamadoEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Times New Roman"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Red),
                                new Point((x * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8, (y * VM.KartyaMagassag) + VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                            //kartyaeletero
                            drawingContext.DrawText(
                                new FormattedText(VM.Kartyak[x, y].EletEro.ToString(), new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Times New Roman"), (int)(VM.KartyaMagassag / 15 + 1), Brushes.Green),
                                new Point((x * VM.KartyaSzelesseg) + VM.KartyaSzelesseg / 8 * 5, (y * VM.KartyaMagassag) + VM.KartyaMagassag - (VM.KartyaMagassag / 8)));
                            //kartyakép
                            drawingContext.DrawImage(VM.Kartyak[x, y].Kep.ImageSource,
                                new Rect(x * VM.KartyaSzelesseg + VM.KartyaSzelesseg / 8, y * VM.KartyaMagassag + VM.KartyaMagassag / 6, VM.KartyaSzelesseg / 8 * 6, VM.KartyaMagassag / 6 * 4));
                        }





                    }
                }

            }





        }





    }
}
