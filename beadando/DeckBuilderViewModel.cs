﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace beadando
{
    public class Bindable : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OPC([CallerMemberName] string s = "")
        {
            PropertyChanged?.Invoke
                (this, new PropertyChangedEventArgs(s));
        }

    }
   public class DeckBuilderViewModel :Bindable
    {

        Kartya[,] kartyak;
        double abl_mag;
        double abl_szel;
        double kartyaSzelesseg=188;
        double kartyaMagassag;
        int sorokSzama;
        double osszMagassag;
        double sajatMagassag;
        List<Kartya> sajatPakli;
        int paklimeret=0;
        ViewModel VM;

        public DeckBuilderViewModel(double abl_mag, double abl_szel,ViewModel VM)
        {
            this.VM = VM;
            this.SajatPakli = VM.SajatPakli;
            this.sorokSzama = (VM.OsszesKartya.Count / 3) + 1;
            Kartyak = new Kartya[3, sorokSzama];
            this.Abl_mag = abl_mag;
            this.Abl_szel = abl_szel;
            int x = 0;
            for (int i = 0; i < sorokSzama; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (x< VM.OsszesKartya.Count)
                    {
                        kartyak[j, i] = VM.OsszesKartya[x];
                        x++;
                    }
                    
                }
            }
        }

        public Kartya[,] Kartyak
        {
            get
            {
                return kartyak;
            }

            set
            {
                kartyak = value;
            }
        }

        public double Abl_mag
        {
            get
            {
                return abl_mag;
            }

            set
            {
                abl_mag = value;
            }
        }

        public double Abl_szel
        {
            get
            {
                return abl_szel;
            }

            set
            {
                abl_szel = value;
            }
        }

        public double KartyaSzelesseg
        {
            get
            {
                return kartyaSzelesseg;
            }

            set
            {
                kartyaSzelesseg = value;
            }
        }

        public double KartyaMagassag
        {
            get
            {
                return KartyaSzelesseg*1.5;
            }
        }

        public double OsszMagassag
        {
            get
            {
                return KartyaMagassag*sorokSzama+10;
            }
        }

        public List<Kartya> SajatPakli
        {
            get
            {
                return sajatPakli;
            }

            set
            {
                sajatPakli = value;
            }
        }

        public double SajatMagassag
        {
            get
            {
                return sajatPakli.Count*KartyaMagassag;
            }
            
        }

        public int Paklimeret
        {
            get
            {
                return paklimeret;
            }

            set
            {
                paklimeret = value;
            }
        }

        public void sajathozAd(double X, double Y)
        {
            int x = (int)(X / kartyaSzelesseg);
            int y = (int)(Y / KartyaMagassag);
            //int hanyszar = 0;
            if (Paklimeret < 30 && VM.beleRakhato(SajatPakli,kartyak[x,y]))
            {


               
                    sajatPakli.Add(Kartyak[x, y]);
                    paklimeret++;
                
                

            }
        }
        public void sajatbolKivesz(double X)
        {
            int i = (int)(X / KartyaMagassag);
            SajatPakli.Remove(SajatPakli[i]);
            paklimeret--;

        }
    }
}
