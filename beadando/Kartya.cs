﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace beadando
{
    
    public class Kartya :Bindable
    {
        int id;
        string nev;
        int tamadoEro;
        bool csapda;
        int eletEro;
        bool el;
        ImageBrush kep;
        ImageBrush kartyaHatter;
        bool tamadhat =false;

        public bool Csapda
        {
            get
            {
                return csapda;
            }

            set
            {
                csapda = value;
            }
        }

        public int EletEro
        {
            get
            {
                return eletEro;
            }

            set
            {
                eletEro = value;
            }
        }

        public int TamadoEro
        {
            get
            {
                return tamadoEro;
            }

            set
            {
                tamadoEro = value;
            }
        }

        public string Nev
        {
            get
            {
                return nev;
            }

            set
            {
                nev = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public bool El
        {
            get
            {
                return (eletEro>0);
            }
        }

        public ImageBrush Kep
        {
            get
            {
                return kep;
            }

            set
            {
                kep = value;
            }
        }

        public ImageBrush KartyaHatter
        {
            get
            {
                return kartyaHatter;
            }

            set
            {
                kartyaHatter = value;
            }
        }

        public bool Tamadhat
        {
            get
            {
                return tamadhat;
            }

            set
            {
                tamadhat = value;
            }
        }

        public void Attack(Kartya k)
        {
            k.eletEro -= this.tamadoEro;
            if (this.csapda == false)
            {
                this.eletEro -= k.tamadoEro;
            }
            if (this.eletEro<1)
            {
                this.el = false;
            }
            if (k.eletEro < 1)
            {
                k.el = false;
            }

        }

        public Kartya(int id,string nev,int tamadoEro,int eletEro)
        {
            if (eletEro < 1)
                throw new Exception("Legalább 1 legyen az életereje....");
            this.id = id;
            this.nev = nev;
            this.tamadoEro = tamadoEro;
            this.eletEro = eletEro;
            this.csapda = false;
            this.el = true;
            tamadhat = false;
            Kep = new ImageBrush();
            Kep.ImageSource = new BitmapImage(new Uri("../../kepek/monster" + id+".jpg", UriKind.Relative));
            KartyaHatter = new ImageBrush();
            kartyaHatter.ImageSource = new BitmapImage(new Uri("../../kepek/card.jpg", UriKind.Relative));
        }
        public Kartya(int id, string nev, int tamadoEro)
        {
            this.id = id;
            this.nev = nev;
            this.tamadoEro = tamadoEro;
            this.csapda = true;
            tamadhat = false;
            Kep = new ImageBrush();
            Kep.ImageSource = new BitmapImage(new Uri("../../kepek/csapda" + id + ".jpg", UriKind.Relative));
            KartyaHatter = new ImageBrush();
            kartyaHatter.ImageSource = new BitmapImage(new Uri("../../kepek/trap.jpg", UriKind.Relative));
        }
    }
}
