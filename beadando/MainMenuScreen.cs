﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace beadando
{
    class MainMenuScreen :FrameworkElement
    {
        
        MainMenuViewModel VM;

        public void VmBeallitas(MainMenuViewModel VM)
        {
            this.VM = VM;
            this.ReDraw();
        }
        public void ReDraw()
        {
            this.InvalidateVisual();
        }
        protected override void OnRender(DrawingContext drawingContext)
        {
            drawingContext.DrawRectangle(Brushes.Gray, null, new Rect(0, 0, this.ActualWidth, this.ActualHeight));
            if (VM != null)
            {
                drawingContext.DrawRectangle(VM.BackgroundPicture, null, new Rect(0, 0, this.ActualWidth, this.ActualHeight));
                drawingContext.DrawRectangle(Brushes.SandyBrown, new Pen(Brushes.Black, 5), new Rect(VM.Szelesseg / 2 - VM.TileSzelesseg / 2, VM.Magassag / 10 * 2, VM.TileSzelesseg, VM.TileMagassag));
                drawingContext.DrawText(
                    new FormattedText("Csata!", new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Times New Roman"), 17, Brushes.Red),
                    new Point(VM.Szelesseg / 2 - 20, VM.Magassag / 10 * 2 + 5));

                drawingContext.DrawRectangle(Brushes.SandyBrown, new Pen(Brushes.Black, 5), new Rect(VM.Szelesseg / 2 - VM.TileSzelesseg / 2, VM.Magassag / 10 * 4, VM.TileSzelesseg, VM.TileMagassag));
                drawingContext.DrawText(
                    new FormattedText("Pakli", new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Times New Roman"), 17, Brushes.Red),
                    new Point(VM.Szelesseg / 2 - 18, VM.Magassag / 10 * 4 + 5));

                drawingContext.DrawRectangle(Brushes.SandyBrown, new Pen(Brushes.Black, 5), new Rect(VM.Szelesseg / 2 - VM.TileSzelesseg / 2, VM.Magassag / 10 * 7, VM.TileSzelesseg, VM.TileMagassag));
                drawingContext.DrawText(
                    new FormattedText("Exit", new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Times New Roman"), 17, Brushes.Red),
                    new Point(VM.Szelesseg / 2 - 16, VM.Magassag / 10 * 7 + 5));
            }
            }


    }
}
