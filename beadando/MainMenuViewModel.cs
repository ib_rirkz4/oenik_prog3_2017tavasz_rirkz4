﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace beadando
{
    class KattintottEventArg : EventArgs
    {
        string gomb;
        public KattintottEventArg(string gomb)
        {
            this.gomb = gomb;
        }

        public string Gomb
        {
            get
            {
                return gomb;
            }

            set
            {
                gomb = value;
            }
        }
    }
    class MainMenuViewModel :Bindable
    {
        ImageBrush backgroundPicture = new ImageBrush();
       
        double tileMagassag = 30;
        double tileSzelesseg = 50;
        double magassag;
        double szelesseg;
        public event EventHandler<KattintottEventArg> kattintott;
        public double TileMagassag
        {
            get
            {
                return tileMagassag;
            }

            set
            {
                tileMagassag = value;
            }
        }

        public double TileSzelesseg
        {
            get
            {
                return tileSzelesseg;
            }

            set
            {
                tileSzelesseg = value;
            }
        }

        public double Magassag
        {
            get
            {
                return magassag;
            }

            set
            {
                magassag = value;
            }
        }

        public double Szelesseg
        {
            get
            {
                return szelesseg;
            }

            set
            {
                szelesseg = value;
            }
        }

        public ImageBrush BackgroundPicture
        {
            get
            {
                return backgroundPicture;
            }

            set
            {
                backgroundPicture = value;
            }
        }
        public void GomValasztas(double mouseX, double mouseY)
        {
            if (mouseX>(szelesseg/2-tileSzelesseg/2) &&mouseX<szelesseg/2+tileSzelesseg/2)
            {
                if (mouseY > magassag/10*2 && mouseY < magassag / 10 * 2+tileMagassag)
                {
                    kattintott?.Invoke(this, new KattintottEventArg("Csata!"));
                }
                else if (mouseY > magassag / 10 * 4 && mouseY < magassag / 10 * 4 + tileMagassag)
                {
                    kattintott?.Invoke(this, new KattintottEventArg("Pakli"));
                }
                else if (mouseY > magassag / 10 * 7 && mouseY < magassag / 10 * 7 + tileMagassag)
                {
                    kattintott?.Invoke(this, new KattintottEventArg("Exit"));
                }
                    else
                {

                } 
            }
        }

        public MainMenuViewModel(double magassag,double szelesseg)
        {
            this.magassag = magassag;
            this.szelesseg = szelesseg;
            this.backgroundPicture.ImageSource = new BitmapImage(new Uri("../../kepek/background.jpg", UriKind.Relative));
        }

    }
}
