﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace beadando
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// public ViewModel VM
    /// 
    
    public partial class MainWindow : Window
    {
        DeckBuilder DB;
        Csatater CST;
        MainMenuViewModel MVM;
        ViewModel VM = new ViewModel();
        CsataterViewModel CSVM;
        public MainWindow()
        {
            InitializeComponent();
            
            
           
            


        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            MVM = new MainMenuViewModel(screen.ActualHeight,screen.ActualWidth);
            screen.VmBeallitas(MVM);
            screen.ReDraw();
            
            MVM.kattintott += MVM_kattintott;

        }

        private void MVM_kattintott(object sender, KattintottEventArg e)
        {
            switch (e.Gomb)
            {
                case "Csata!":
                    try
                    {
                        CSVM = new CsataterViewModel(VM);
                        CST = new Csatater(CSVM);
                        CST.Show();
                    }
                    catch (Exception asd)
                    {
                        if (asd.Message == "pakliMeret")
                        {
                            MessageBox.Show("Nem elég nagy a paklid!", "Hiba", MessageBoxButton.OK);
                        }
                    }

                    break;
                case "Pakli":
                    DB = new DeckBuilder(VM);
                    DB.Show();
                    break;
                case "Exit":
                    
                    this.Close();
                    
                    break;
                default:
                    break;
            }
        }

        private void screen_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MVM.GomValasztas(e.GetPosition(screen).X,e.GetPosition(screen).Y);
        }
    }
}
