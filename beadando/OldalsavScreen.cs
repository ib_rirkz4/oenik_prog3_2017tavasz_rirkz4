﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace beadando
{
    class OldalsavScreen : FrameworkElement
    {
        string hibauzenet;
        CsataterViewModel VM;
        ImageBrush mybrush = new ImageBrush();
        string sajatPaklimeret="";
        string ellenfelPaklimeret="";

        public void HibauzenetBeallit(string hibauzenet)
        {
            this.hibauzenet = hibauzenet;
            this.InvalidateVisual();
        }
        public void SajatPakliMereteKiiras(string paklimeret)
        {
            this.sajatPaklimeret = paklimeret;
            this.InvalidateVisual();
        }
        public void EllenfelPakliMereteKiiras(string paklimeret)
        {
            this.ellenfelPaklimeret = paklimeret;
            this.InvalidateVisual();
        }

        public void VMBeallit(CsataterViewModel VM)
        {
            this.VM = VM;
            this.InvalidateVisual();
            mybrush.ImageSource = new BitmapImage(new Uri("../../kepek/cardb.jpg", UriKind.Relative));
        }
        protected override void OnRender(DrawingContext drawingContext)
        {
            drawingContext.DrawRectangle(Brushes.LightBlue, null, new Rect(0, 0, this.ActualWidth, this.ActualHeight));
            if (hibauzenet != null)
            {
                //hibaüzenet kirajzolása
                drawingContext.DrawText(
                                    new FormattedText(hibauzenet, new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Segoe UI"), 24, Brushes.Red),
                                    new Point(10.0, 5.0));
                //kör átadás gomb
                drawingContext.DrawRectangle(Brushes.Blue, new Pen(Brushes.Black, 5), new Rect(this.ActualWidth / 4, this.ActualHeight / 2 - 25, 130, 50));
                //kör átadás felirata
                drawingContext.DrawText(
                                    new FormattedText("END TURN", new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), 32, Brushes.Green),
                                    new Point((this.ActualWidth / 4 + 5), this.ActualHeight / 2 - 19));

                //exit gomb
                drawingContext.DrawRectangle(Brushes.Black, null, new Rect(this.ActualWidth / 2 - 25, this.ActualHeight - 40, 50, 30));
                //exit felirata
                drawingContext.DrawText(
                                    new FormattedText("EXIT", new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), 20, Brushes.Red),
                                    new Point((this.ActualWidth / 2 - 17), this.ActualHeight - 38));

                //sajat pakli
                drawingContext.DrawRectangle(mybrush, null, new Rect(this.ActualWidth / 2 - 50, this.ActualHeight / 6 * 4, 100, 150));
                //sajat pakli meret
                
                drawingContext.DrawText(
                                    new FormattedText(sajatPaklimeret , new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), 18, Brushes.Black),
                                    new Point(this.ActualWidth / 2 +60, this.ActualHeight / 6 * 4 +40));
                //ellenfel pakli
                drawingContext.DrawRectangle(mybrush, null, new Rect(this.ActualWidth / 2 - 50, this.ActualHeight / 6 * 1, 100, 150));
                //ellenfel pakli meret

                drawingContext.DrawText(
                                    new FormattedText(ellenfelPaklimeret, new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), 18, Brushes.Black),
                                    new Point(this.ActualWidth / 2 + 60, this.ActualHeight / 6 * 1 + 40));

                //sajat elet
                drawingContext.DrawText(
                                   new FormattedText(VM.SajatEletero + "/20", new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), 20, Brushes.Green),
                                   new Point( 15, this.ActualHeight - 50));

                //ellenfel elet     
                drawingContext.DrawText(
                                   new FormattedText(VM.EllenfelEletero + "/20", new System.Globalization.CultureInfo(1), new FlowDirection(), new Typeface("Impact"), 20, Brushes.Green),
                                   new Point(15,  50));
            }
        }
    }
}
