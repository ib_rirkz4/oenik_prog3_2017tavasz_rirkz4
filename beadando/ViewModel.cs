﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace beadando
{
    public class ViewModel 
    {
        Random r = new Random();
        List<Kartya> osszesKartya;
        List<Kartya> sajatPakli;
        List<Kartya> ellenfelPakli;
    
        /// <summary>
        /// Az ellenfél pakliját véletlen rakja a megadott szabályok alapján. EGy játékmenet alatt folyton ugyna az lesz az ellenfél paklija
        /// </summary>
        /// <returns></returns>
        public List<Kartya> PakliGeneral()
        {
            List<Kartya> pakli = new List<Kartya>();
            Kartya kijelolt;
            while (pakli.Count < 30)
            {
                kijelolt = osszesKartya[R.Next(0, osszesKartya.Count)];
                if (beleRakhato(pakli,kijelolt))
                {
                    pakli.Add(kijelolt);
                }

            }

            return pakli;
        }
        public bool beleRakhato(List<Kartya> kartyak, Kartya kartya)
        {
            bool mehet;

            int hanyszor = 0;
            foreach (Kartya item in kartyak)
            {
                if (item == kartya)
                {
                    hanyszor++;
                }
            }
            if (hanyszor<2)
            {
                mehet = true;
            }
            else
            {
                mehet = false;
            }
            return mehet;
        }
        public List<Kartya> OsszesKartya
        {
            get
            {
                return osszesKartya;
            }

            set
            {
                osszesKartya = value;
            }
        }

        public List<Kartya> SajatPakli
        {
            get
            {
                return sajatPakli;
            }

            set
            {
                sajatPakli = value;
            }
        }

        public List<Kartya> EllenfelPakli
        {
            get
            {
                return ellenfelPakli;
            }

            set
            {
                ellenfelPakli = value;
            }
        }

        public Random R
        {
            get
            {
                return r;
            }

            set
            {
                r = value;
            }
        }

        public ViewModel()
        {
            osszesKartya = new List<Kartya>();
            sajatPakli = new List<Kartya>();
            
      
            osszesKartya.Add(new Kartya(1,"B.E.Hó Pepe",9999,9999));
            osszesKartya.Add(new Kartya(2, "Green Frank", 500, 2000));
            osszesKartya.Add(new Kartya(3, "The screem", 1500, 1000));
            osszesKartya.Add(new Kartya(4, "F.G.M. Pepe", 2000, 100));
            osszesKartya.Add(new Kartya(5, "Crusader", 1200, 500));
            osszesKartya.Add(new Kartya(6, "Tea Kermit", 100, 5000));
            osszesKartya.Add(new Kartya(7, "Grumpy Cat", 600, 700));
            osszesKartya.Add(new Kartya(8, "1M Dollar", 1300, 400));
            osszesKartya.Add(new Kartya(9, "Hmm.", 600, 600));
            osszesKartya.Add(new Kartya(10, "This kids..", 850, 600));
            osszesKartya.Add(new Kartya(11, "Am I bad?", 200, 300));
            osszesKartya.Add(new Kartya(12, "Rage Pepe", 100, 100));
            osszesKartya.Add(new Kartya(13, "Weed Pepe", 850, 900));
            osszesKartya.Add(new Kartya(14, "What?!", 1000));
            osszesKartya.Add(new Kartya(15, "Time Heals", 500));
            osszesKartya.Add(new Kartya(16,"Triggered",2000));
            ellenfelPakli = PakliGeneral();
            sajatPakli = PakliGeneral();

        }
    }
}
